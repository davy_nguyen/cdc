function initData() {
  jimData.variables["0"] = "0";
  jimData.variables["qui_afficher"] = "0000";
  jimData.variables["Compétence_select_eleve"] = "";
  jimData.variables["liste_affichee"] = "";
  jimData.variables["eleve_selectionne"] = "Jean Dupont";
  jimData.variables["Compétence_select_all"] = "";
  jimData.variables["Activité_select_all"] = "";
  jimData.variables["Activité_select_eleve"] = "";
  jimData.datamasters["Comp_2_012"] = [
    {
      "id": 1,
      "datamaster": "Comp_2_012",
      "userdata": {
        "Nom": "Lacaille",
        "Prénom": "Thierry",
        "Acquisition": "./images/79cb2c45-434c-425a-a5e2-d6ad3863d79d.png"
      }
    },
    {
      "id": 2,
      "datamaster": "Comp_2_012",
      "userdata": {
        "Nom": "Li",
        "Prénom": "Marc",
        "Acquisition": "./images/9110847d-aa18-45fd-be0e-a8d2a5df469c.png"
      }
    },
    {
      "id": 3,
      "datamaster": "Comp_2_012",
      "userdata": {
        "Nom": "Marchand",
        "Prénom": "Gabriel",
        "Acquisition": "./images/21b69850-03ba-4b2d-8b77-5c4759636824.png"
      }
    },
    {
      "id": 4,
      "datamaster": "Comp_2_012",
      "userdata": {
        "Nom": "Martin",
        "Prénom": "Charlotte",
        "Acquisition": "./images/7960f4fd-7cd3-4ee0-ac1e-6d5243693418.png"
      }
    },
    {
      "id": 5,
      "datamaster": "Comp_2_012",
      "userdata": {
        "Nom": "Mattei",
        "Prénom": "Lucas",
        "Acquisition": "./images/21f5b25f-1483-480d-8f0c-63393fb7ce3d.png"
      }
    }
  ];

  jimData.datamasters["Comp_2_show"] = [
  ];

  jimData.datamasters["Comp_1_012"] = [
    {
      "id": 1,
      "datamaster": "Comp_1_012",
      "userdata": {
        "Nom": "Lacaille",
        "Prénom": "Thierry",
        "Acquisition": "./images/79cb2c45-434c-425a-a5e2-d6ad3863d79d.png"
      }
    }
  ];

  jimData.datamasters["Classe 1"] = [
    {
      "id": 1,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Abadi",
        "Prénom": "Abdel",
        "Acquisition globale": "./images/6091e8b9-e05a-482e-9a10-b061324205cf.png"
      }
    },
    {
      "id": 2,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Belair",
        "Prénom": "Didier",
        "Acquisition globale": "./images/a3f28cc7-dbd5-40ae-a671-d4b5b3bf6247.png"
      }
    },
    {
      "id": 3,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Boissard",
        "Prénom": "Chloé",
        "Acquisition globale": "./images/aad1c5d4-9ddb-4185-ba81-cc0b3c13d669.png"
      }
    },
    {
      "id": 4,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Dupont",
        "Prénom": "Jean",
        "Acquisition globale": "./images/9ad220c2-60f5-457c-b12f-bb64e2dfaeed.png"
      }
    },
    {
      "id": 5,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Faubert",
        "Prénom": "Maurice",
        "Acquisition globale": "./images/289317e2-4c92-4f4f-9ac9-786377917435.png"
      }
    },
    {
      "id": 6,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Fernandez",
        "Prénom": "Lucie",
        "Acquisition globale": "./images/4a40eb95-571c-4615-8f66-bc1883bfab80.png"
      }
    },
    {
      "id": 7,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Franchet",
        "Prénom": "Cécile",
        "Acquisition globale": "./images/887dad21-db46-4e5e-bf64-1b4c55bcddc2.png"
      }
    },
    {
      "id": 8,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Gaudreau",
        "Prénom": "Maëva",
        "Acquisition globale": "./images/af181585-7657-4508-bfbb-dd00e2831002.png"
      }
    },
    {
      "id": 9,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "George",
        "Prénom": "Lucas",
        "Acquisition globale": "./images/3ae944f3-7a01-4369-9787-0a22878d89b3.png"
      }
    },
    {
      "id": 10,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Kadir",
        "Prénom": "Sarah",
        "Acquisition globale": "./images/a0f03678-c2bd-479d-8a68-8ca46675b36d.png"
      }
    },
    {
      "id": 11,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Lacaille",
        "Prénom": "Thierry",
        "Acquisition globale": "./images/137a6294-e3d3-4cd0-886a-fc57adf2da9e.png"
      }
    },
    {
      "id": 12,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Li",
        "Prénom": "Marc",
        "Acquisition globale": "./images/814ccc71-c074-43dc-aac4-05d2a0c35acf.png"
      }
    },
    {
      "id": 13,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Marchand",
        "Prénom": "Gabriel",
        "Acquisition globale": "./images/af13a65d-60be-4075-9836-177870316b54.png"
      }
    },
    {
      "id": 14,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Martin",
        "Prénom": "Charlotte",
        "Acquisition globale": "./images/e2193987-15da-4a5d-9cf5-34b13d0ac8ef.png"
      }
    },
    {
      "id": 15,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Mattei",
        "Prénom": "Lucas",
        "Acquisition globale": "./images/d77a7578-ddb7-492c-800f-4495f4252bf3.png"
      }
    },
    {
      "id": 16,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Nguyen",
        "Prénom": "Lola",
        "Acquisition globale": "./images/3ecd640f-b60a-41bd-a211-516c4a15157a.png"
      }
    },
    {
      "id": 17,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Rodrigues",
        "Prénom": "Arthur",
        "Acquisition globale": "./images/6c34ae68-6b19-4c67-b065-86cc9cdd03b8.png"
      }
    },
    {
      "id": 18,
      "datamaster": "Classe 1",
      "userdata": {
        "Nom": "Tran",
        "Prénom": "Tom",
        "Acquisition globale": "./images/ab202f5d-e433-49b8-ba30-ff508883e1bf.png"
      }
    }
  ];

  jimData.datamasters["Comp_2_all"] = [
    {
      "id": 1,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Abadi",
        "Prénom": "Abdel",
        "Acqtion": "./images/6091e8b9-e05a-482e-9a10-b061324205cf.png",
        "Acquisition_val": "68"
      }
    },
    {
      "id": 2,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Belair",
        "Prénom": "Didier",
        "Acqtion": "./images/b9f34d3e-4a6c-4bc7-ae75-8d41aca14975.png",
        "Acquisition_val": "0"
      }
    },
    {
      "id": 3,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Boissard",
        "Prénom": "Chloé",
        "Acqtion": "./images/aad1c5d4-9ddb-4185-ba81-cc0b3c13d669.png",
        "Acquisition_val": "36"
      }
    },
    {
      "id": 4,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Dupont",
        "Prénom": "Jean",
        "Acqtion": "./images/9ad220c2-60f5-457c-b12f-bb64e2dfaeed.png",
        "Acquisition_val": "85"
      }
    },
    {
      "id": 5,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Faubert",
        "Prénom": "Maurice",
        "Acqtion": "./images/289317e2-4c92-4f4f-9ac9-786377917435.png",
        "Acquisition_val": "50"
      }
    },
    {
      "id": 6,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Fernandez",
        "Prénom": "Lucie",
        "Acqtion": "./images/4a40eb95-571c-4615-8f66-bc1883bfab80.png",
        "Acquisition_val": "67"
      }
    },
    {
      "id": 7,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Franchet",
        "Prénom": "Cécile",
        "Acqtion": "./images/887dad21-db46-4e5e-bf64-1b4c55bcddc2.png",
        "Acquisition_val": "69"
      }
    },
    {
      "id": 8,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Gaudreau",
        "Prénom": "Maëva",
        "Acqtion": "./images/af181585-7657-4508-bfbb-dd00e2831002.png",
        "Acquisition_val": "67"
      }
    },
    {
      "id": 9,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "George",
        "Prénom": "Lucas",
        "Acqtion": "./images/3ae944f3-7a01-4369-9787-0a22878d89b3.png",
        "Acquisition_val": "91"
      }
    },
    {
      "id": 10,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Kadir",
        "Prénom": "Sarah",
        "Acqtion": "./images/a0f03678-c2bd-479d-8a68-8ca46675b36d.png",
        "Acquisition_val": "89"
      }
    },
    {
      "id": 11,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Lacaille",
        "Prénom": "Thierry",
        "Acqtion": "./images/137a6294-e3d3-4cd0-886a-fc57adf2da9e.png",
        "Acquisition_val": "68"
      }
    },
    {
      "id": 12,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Li",
        "Prénom": "Marc",
        "Acqtion": "./images/814ccc71-c074-43dc-aac4-05d2a0c35acf.png",
        "Acquisition_val": "51"
      }
    },
    {
      "id": 13,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Marchand",
        "Prénom": "Gabriel",
        "Acqtion": "./images/af13a65d-60be-4075-9836-177870316b54.png",
        "Acquisition_val": "67"
      }
    },
    {
      "id": 14,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Martin",
        "Prénom": "Charlotte",
        "Acqtion": "./images/e2193987-15da-4a5d-9cf5-34b13d0ac8ef.png",
        "Acquisition_val": "68"
      }
    },
    {
      "id": 15,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Mattei",
        "Prénom": "Lucas",
        "Acqtion": "./images/d77a7578-ddb7-492c-800f-4495f4252bf3.png",
        "Acquisition_val": "55"
      }
    },
    {
      "id": 16,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Nguyen",
        "Prénom": "Lola",
        "Acqtion": "./images/3ecd640f-b60a-41bd-a211-516c4a15157a.png",
        "Acquisition_val": "12"
      }
    },
    {
      "id": 17,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Rodrigues",
        "Prénom": "Arthur",
        "Acqtion": "./images/6c34ae68-6b19-4c67-b065-86cc9cdd03b8.png",
        "Acquisition_val": "42"
      }
    },
    {
      "id": 18,
      "datamaster": "Comp_2_all",
      "userdata": {
        "Nom": "Tran",
        "Prénom": "Tom",
        "Acqtion": "./images/ab202f5d-e433-49b8-ba30-ff508883e1bf.png",
        "Acquisition_val": "81"
      }
    }
  ];

  jimData.isInitialized = true;
}