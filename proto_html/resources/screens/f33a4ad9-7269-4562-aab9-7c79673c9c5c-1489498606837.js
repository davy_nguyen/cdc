jQuery("#simulation")
  .on("click", ".s-f33a4ad9-7269-4562-aab9-7c79673c9c5c .click", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-Text_28")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-act_1"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-p_act-1-012"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_29")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-act_2"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-p_act-2-012"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Button_9")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Compétence_select_all",
                    "value": "1"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/d12245cc-1680-458d-89dd-4f0d7fb22724"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_5")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-liste_act"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-all_eleves"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Line_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-liste_act"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-all_eleves"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_2")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Activité_select_eleve",
                    "value": "11"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Button_10")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Compétence_select_all",
                    "value": "2"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/d12245cc-1680-458d-89dd-4f0d7fb22724"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Button_8")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Compétence_select_all",
                    "value": "1"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/d12245cc-1680-458d-89dd-4f0d7fb22724"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Menu_item_8")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-act_2_gen"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Menu_item_9")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-act_2_types_erreurs"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Menu_item_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-act_2_enonce"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Rectangle_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-liste_act"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-all_eleves"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Line_1")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-liste_act"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-all_eleves"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_92")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_122")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_152")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_362")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Activité_select_eleve",
                    "value": "11"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_392")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Activité_select_eleve",
                    "value": "11"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_422")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Activité_select_eleve",
                    "value": "11"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_272")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Activité_select_eleve",
                    "value": "11"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_302")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Activité_select_eleve",
                    "value": "11"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_332")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Activité_select_eleve",
                    "value": "11"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_4")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/ea605619-b250-4564-b3ff-26af37dc2e98"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_6")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-eleve"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_7")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-classe"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_10")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-classe"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_11")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/ea605619-b250-4564-b3ff-26af37dc2e98"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_12")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-eleve"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_8")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/ea605619-b250-4564-b3ff-26af37dc2e98"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_18")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-eleve"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_52")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/d12245cc-1680-458d-89dd-4f0d7fb22724"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_65")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-liste_act"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_15")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/ea605619-b250-4564-b3ff-26af37dc2e98"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_16")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-eleve"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_68")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/ea605619-b250-4564-b3ff-26af37dc2e98"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_72")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-classe"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_73")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/31037c9f-a43b-4789-b883-5e682b6e05a7"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_75")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/daaa0764-6066-4c7a-9734-9ddc5d269460"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_18")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-classe"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_19")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimNavigation",
                  "parameter": {
                    "target": "screens/ea605619-b250-4564-b3ff-26af37dc2e98"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("pageload", ".s-f33a4ad9-7269-4562-aab9-7c79673c9c5c .pageload", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c")) {
      cases = [
        {
          "blocks": [
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "Activité_select_all"
                },"1" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-act_1"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-p_act-1-012"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            },
            {
              "condition": {
                "action": "jimEquals",
                "parameter": [ {
                  "datatype": "variable",
                  "element": "Activité_select_all"
                },"2" ]
              },
              "actions": [
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-act_2"
                  },
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimShow",
                  "parameter": {
                    "target": "#s-p_act-2-012"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("pageunload", ".s-f33a4ad9-7269-4562-aab9-7c79673c9c5c .pageunload", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getEventFirer();
    if(jFirer.is("#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c")) {
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimSetValue",
                  "parameter": {
                    "variable": "Activité_select_all",
                    "value": "0"
                  },
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      event.data = data;
      jEvent.launchCases(cases);
    }
  })
  .on("mouseenter dragenter", ".s-f33a4ad9-7269-4562-aab9-7c79673c9c5c .mouseenter", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Image_5") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_305": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_305": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_275": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_275": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_6") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_306": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_306": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_276": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_276": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_2") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_302": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_302": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_272": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_272": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_3") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_303": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_303": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_273": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_273": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_4") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_304": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_304": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_274": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_274": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_50") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_395": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_395": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_365": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_365": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_51") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_396": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_396": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_366": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_366": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_48") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_394": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_394": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_364": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_364": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_47") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_393": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_393": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_363": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_363": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Image_46") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_392": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_392": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                },
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_362": {
                      "attributes": {
                        "background-color": "#40FF00",
                        "background-image": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Text_cell_362": {
                      "attributes-ie": {
                        "-pie-background": "#40FF00",
                        "-pie-poll": "false"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_362") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_46 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_46": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_392") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_46 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_46": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_422") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_46 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_46": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_363") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_47 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_47": {
                      "attributes": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_393") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_47 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_47": {
                      "attributes": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_423") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_47 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_47": {
                      "attributes": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_364") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_48 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_48": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_394") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_48 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_48": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_424") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_48 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_48": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_365") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_50 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_50": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_395") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_50 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_50": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_425") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_50 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_50": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_366") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_51 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_51": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_396") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_51 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_51": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_426") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_51 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_51": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_272") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_2 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_2": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_302") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_2 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_2": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_332") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_2 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_2": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_273") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_3 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_3": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_303") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_3 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_3": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_333") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_3 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_3": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_274") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_4 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_4": {
                      "attributes": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_304") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_4 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_4": {
                      "attributes": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_334") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_4 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_4": {
                      "attributes": {
                        "border-top-width": "1px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "1px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "1px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "1px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_275") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_5 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_5": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_305") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_5 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_5": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_335") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_5 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_5": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_276") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_6 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_6": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_306") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_6 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_6": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    } else if(jFirer.is("#s-Text_cell_336") && jFirer.has(event.relatedTarget).length === 0) {
      event.backupState = true;
      event.target = jFirer;
      cases = [
        {
          "blocks": [
            {
              "actions": [
                {
                  "action": "jimChangeStyle",
                  "parameter": [ {
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_6 > svg": {
                      "attributes": {
                        "overlay": "none"
                      }
                    }
                  },{
                    "#s-f33a4ad9-7269-4562-aab9-7c79673c9c5c #s-Image_6": {
                      "attributes": {
                        "border-top-width": "2px",
                        "border-top-style": "solid",
                        "border-top-color": "#40FF00",
                        "border-right-width": "2px",
                        "border-right-style": "solid",
                        "border-right-color": "#40FF00",
                        "border-bottom-width": "2px",
                        "border-bottom-style": "solid",
                        "border-bottom-color": "#40FF00",
                        "border-left-width": "2px",
                        "border-left-style": "solid",
                        "border-left-color": "#40FF00",
                        "border-radius": "14px 14px 14px 14px"
                      }
                    }
                  } ],
                  "exectype": "serial",
                  "delay": 0
                }
              ]
            }
          ],
          "exectype": "serial",
          "delay": 0
        }
      ];
      jEvent.launchCases(cases);
    }
  })
  .on("mouseleave dragleave", ".s-f33a4ad9-7269-4562-aab9-7c79673c9c5c .mouseleave", function(event, data) {
    var jEvent, jFirer, cases;
    if(data === undefined) { data = event; }
    jEvent = jimEvent(event);
    jFirer = jEvent.getDirectEventFirer(this);
    if(jFirer.is("#s-Image_5")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_6")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_2")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_3")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_4")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_50")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_51")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_48")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_47")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Image_46")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_362")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_392")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_422")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_363")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_393")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_423")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_364")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_394")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_424")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_365")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_395")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_425")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_366")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_396")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_426")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_272")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_302")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_332")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_273")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_303")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_333")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_274")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_304")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_334")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_275")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_305")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_335")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_276")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_306")) {
      jEvent.undoCases(jFirer);
    } else if(jFirer.is("#s-Text_cell_336")) {
      jEvent.undoCases(jFirer);
    }
  });