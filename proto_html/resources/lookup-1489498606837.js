(function(window, undefined) {
  var dictionary = {
    "31037c9f-a43b-4789-b883-5e682b6e05a7": "Compétences_eleve",
    "f33a4ad9-7269-4562-aab9-7c79673c9c5c": "Activités_classe_all",
    "d12245cc-1680-458d-89dd-4f0d7fb22724": "Compétences_classe_all",
    "daaa0764-6066-4c7a-9734-9ddc5d269460": "Activités_eleve",
    "ea605619-b250-4564-b3ff-26af37dc2e98": "Homepage",
    "5306a60c-133b-4095-92b3-019c36b79259": "COLORTEST",
    "87db3cf7-6bd4-40c3-b29c-45680fb11462": "960 grid - 16 columns",
    "e5f958a4-53ae-426e-8c05-2f7d8e00b762": "960 grid - 12 columns",
    "f39803f7-df02-4169-93eb-7547fb8c961a": "Template 1",
    "bb8abf58-f55e-472d-af05-a7d1bb0cc014": "default"
  };

  var uriRE = /^(\/#)?(screens|templates|masters|scenarios)\/(.*)(\.html)?/;
  window.lookUpURL = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, url;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      url = folder + "/" + canvas;
    }
    return url;
  };

  window.lookUpName = function(fragment) {
    var matches = uriRE.exec(fragment || "") || [],
        folder = matches[2] || "",
        canvas = matches[3] || "",
        name, canvasName;
    if(dictionary.hasOwnProperty(canvas)) { /* search by name */
      canvasName = dictionary[canvas];
    }
    return canvasName;
  };
})(window);