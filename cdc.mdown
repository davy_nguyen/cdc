# SOMMAIRE


### 1. Presentation du projet

1. contexte
2. présentation du dashboard
3. objectifs


### 2. Besoins


1. menu de navigation principal et barre de filtre
2. page d'accueil	
3. vue des activités par classe
4. vue des activités pour un eleve
5. critère d'acceptabilité du dashboard

### 3.Déroulement du projet

1. planification
2. outil mis a disposition
3. technologies utilisées

---


# 1. Présentation du projet

### 1.1. Contexte

Ce projet se déroule dans le cadre de l'UE Projet de première année de master ANDROIDE de l'université Pierre et Marie Curie (UPMC). La finalité de ce projet est d'établir un tableau de bord, aussi couramment appelé dashboard, dynamique sur une page pour des enseignants. Ce tableau de bord sera connecté à divers outils numériques afin de répondre aux besoins de l'enseignant, qui permettra entre autre de lui permettre de mieux comprendre ou en est sa classe.

### 1.2. Présentation du tableau de bord

Le dashboard sera une page web contenant une collection de vues reliées entre elles. Cette collection contiendra notamment: 
 
* une page d'accueil qui regroupera plusieurs informations (favoris, activités récentes,etc)
* une vue pour une classe qui contiendra les données de tout un groupe d'élèves (moyenne, etc)
* une vue pour un élève qui contiendra les données d'un élève en particulier (notes, etc)

Ces deux dernières vues pourront comprendre plusieurs sous onglets et seront donc des menus déroulants. Mais dans le cadre de ce projet, seulement un sous onglet sera implémenté: les activités (par classes et par élèves).  
On pourra, entre autres, cliquer sur une activité particulière afin d'avoir plus de détail sur cette activité, divers graphes,..

### 1.3. Objectifs

L'objectif principal de ce dashboard est de permettre à l'utilisateur de trouver les informations qui l'intéressent **facilement** et **rapidement**. Il devra aussi être adaptatif, c'est-à-dire qui devra être fonctionnel sur plusieurs types d'appareil (navigateurs web, tablettes, téléphones,..). Il devra de plus être dynamique, afin de permettre aux utilisateurs de pouvoir filtrer et voir précisément les données qui les intéressent.  
Dans le cadre de ce projet, nous reproduirons partiellement un prototype conçu avec Justinmind qui nous sera fourni (voir 3.2. Outils mis à disposition) avec bootstrap4.

# 2. Besoins

### 2.1. Menu de navigation principal et barre de filtre

La barre de filtre sera en haut de page et toujours visible. Dans le cadre du projet, elle sera visible mais non fonctionnelle.  
Le menu de navigation sera latéral et toujours visible. Il permettra de naviguer rapidement et facilement entre les différentes vues et contiendra 3 éléments:

* un lien vers la page d'accueil
* un lien vers la vue classe
* un lien vers la vue élève

Les deux derniers liens seront dépliables afin d'afficher/masquer les sous menus disponibles. Comme annoncé plus haut dans la présentation du tableau de bord, il n'y en aura seulement un pour chaque: les sous menus activité (un pour la vue classe et un pour la vue élève). On peut imaginer qu'il y aura d'autres sous menus qui seront ajoutés par la suite du développement du dashboard.

### 2.2. Page d'accueil

La page d'accueil regroupera divers informations comme les vues favorites, les activités récentes, l'historique,..  
Dans le cadre du projet, nous ne nous attarderons pas sur cette page et sera juste statique.

### 2.3. Vue des activités par classe

Cette vue est divisée en 2 parties:

* la liste des activités à gauche ainsi que la réussite moyenne et la durée moyenne qui leur sont associées
* la liste des élèves à droite ainsi que la réussite moyenne et le temps cumulé qui leur sont associés 

De plus, on pourra cliquer sur une activité afin d'obtenir les détails concernant cette activité en particulier. En plus des informations déjà affichées, il y aura un histogramme permettant de visualiser les données de la classe. **Cet histogramme devra permettre de filtrer les élèves affichés dans la partie droite en fonction de la réussite.**  
On pourra revenir à la liste des activités à l'aide d'un bouton de retour en arrière.

On pourra aussi à tout moment cliquer sur un élève parmi la liste des élèves afin d'arriver à sa page personnelle. En fonction de la partie activité actuelle (liste des activités ou une activité particulière), on arrivera soit sur la vue de l'élève générale (liste des activités) soit sur la vue de l'élève concernant une activité particulière (voir 2.4. Vue des activités par élève).

### 2.4. Vue des activités par élève

Cette vue sera divisée en 2 parties:

* la liste des activités ainsi que la réussite et la temps passée dessus par l'élève
* une petite présentation de l'élève (nom, prénom, ..) ainsi qu'une barre déroulante permettant de changer d'élève

La liste des activités fonctionnera de manière similaire à celle dans la vue des activités par classe, sauf qu'elle concernera un élève en particulier. On pourra donc de même manière accédé aux détails d'une activité mais elle contiendra seulement les données concernant l'élève en question.

### 2.5. Critère d'acceptabilité du dashboard

* les graphes (histogrammes) devront être dynamique et doivent pouvoir agir en tant que filtre sur la liste des élèves
* les différentes vues doivent être accessibles à partir du menu de navigation ou des différents liens avec les autres vues
* l'outil devra être adaptatif, c'est-à-dire qu'il pourra être utilisé sur une large gamme de support différent avec le même confort


# 3. Déroulement du projet

### 3.1. Planification

Février: 

* brainstorming  

Mars:

* 20 mars: prototype Justinmind fini et reçu
* 27 mars: fin de rédaction du cahier des charges (premiere version)
* 31 mars - 5 avril: apprentissage bootstrap4

Avril:

* 6 avril - 12 avril: présentation globale de la page et menu de navigation fonctionnel
* 13 avril - 18 avril: écriture des vues par classe et ajout des liens (entres elles et dans le menu)
* 19 avril - 25 avril: écriture des vues pour les élèves et ajout des liens (entres elles et dans le menu)
* 26 avril - 4 mai: ajout des diagrammes dynamiques

Mai:

* 5 mai - 12 mai: ajout des liens entre les vues élève et classe et ajout de la barre de filtre

### 3.2. Outils mis à disposition

* le prototype Justinmind (voir index.html dans le dossier proto_html)
* fausses données au format JSON (liste des activités et d'élèves, les notes et temps, ..)

### 3.3. Technologies utilisées

* bootstrap4: bibliotheque css qui permet de simplifier le design de site web grâce à des designs préconstruits
* chartist.js: librairie permettant d'intégrer divers graphes
